* Ehsan Jahangirzadeh 810194554
* CA4 VLSI

**** Load libraries ****
.prot
.lib 'crn90g_2d5_lk_v1d2p1.l' TT_lvt
.unprot


**** Parameters ****
.param			Lmin=100n
+beta=2
+Wp='2*Lmin*beta'
+Wn='2*Lmin'
+Vdd=1V

**** Source Voltage ****

VSupply		Vs		gnd		DC		Vdd
Vin1		  clk   0     Pulse 1V  0  0   50pS  50pS  2nS  4nS

**** Subkits ****
.SUBCKT		Inv	  in    out	    gnd	  Vs
MIp		    out		in		Vs			Vs		pch_lvt		l='Lmin'		w='Wp'	
MIn		    out   in  	gnd   	gnd   nch_lvt  	l='Lmin' 		w='Wn'
.ENDS	Inv

.SUBCKT		XOR		A  	  B		    out		gnd		    Vs
MXp1		  X		  BB		Vs			Vs		pch_lvt		l='Lmin'		w='2*Wp'
MXp2		  out		A		  X			  X		  pch_lvt		l='Lmin'		w='2*Wp'
MXp3		  Y		  B		  Vs			Vs		pch_lvt		l='Lmin'		w='2*Wp'
MXp4		  out		AB		Y		  	Y		  pch_lvt		l='Lmin'		w='2*Wp'
MXn1		  Z	  	A		  gnd			gnd		nch_lvt		l='Lmin'		w='2*Wn'
MXn2		  out		B		  Z			  Z		  nch_lvt		l='Lmin'		w='2*Wn'
MXn3		  W		  AB		gnd			gnd		nch_lvt		l='Lmin'		w='2*Wn'
MXn4		  out		BB		W		  	W		  nch_lvt		l='Lmin'		w='2*Wn'

XxorInv1	A		  AB		gnd		  Vs		Inv
XxorInv2	B		  BB		gnd		  Vs		Inv
.ENDS		XOR

.SUBCKT		NAND	A  	  B		    out		gnd		    Vs
MNp1		  out		A		  Vs			Vs		pch_lvt		l='Lmin'		w='Wp'
MNp2		  out		B		  Vs			Vs		pch_lvt		l='Lmin'		w='Wp'	
MNn1		  X    	A  	  gnd   	gnd   nch_lvt  	l='Lmin' 		w='2*Wn'
MNn2		  out   B  	  X   		X  		nch_lvt  	l='Lmin' 		w='2*Wn'
.ENDS	NAND

.SUBCKT		DFF		D		  QS		  clk		gnd		    Vs
XdffInv1	clk		clkB		gnd		Vs		Inv

MDFFp1		QM		clkB	QMBB	QMBB	pch_lvt		l='Lmin'		w='Wp'
MDFFn1		QM		clk		QMBB	QMBB	nch_lvt		l='Lmin'		w='Wn'
MDFFp2		D		  clk		QMBB	QMBB	pch_lvt		l='Lmin'		w='Wp'
MDFFn2		D		  clkB	QMBB	QMBB	nch_lvt		l='Lmin'		w='Wn'
XdffInv2	QMBB	QMB		gnd		Vs		Inv
XdffInv3	QMB		QM		gnd		Vs		Inv

MDFFp3		QS		clk		QSBB	QSBB	pch_lvt		l='Lmin'		w='Wp'
MDFFn3		QS		clkB	QSBB	QSBB	nch_lvt		l='Lmin'		w='Wn'
MDFFp4		QM		clkB	QSBB	QSBB	pch_lvt		l='Lmin'		w='Wp'
MDFFn4		QM		clk		QSBB	QSBB	nch_lvt		l='Lmin'		w='Wn'
XdffInv4	QSBB	QSB		gnd		Vs		Inv
XdffInv5	QSB		QS		gnd		Vs		Inv
.ENDS	DFF 

.SUBCKT		HA		  A		  B		    Cout	Sum		    gnd		      Vs
XhaNand	  A  		  B		  Cout_b	gnd		Vs		    NAND
XhaInv	  Cout_b	Cout	gnd		  Vs		Inv
XhaXor	  A  		  B		  Sum		  gnd		Vs		    XOR
.ENDS		HA 

.SUBCKT		FA		A		B		Cin		Cout	Sum		gnd		Vs
Mfap1		  X		    A		  Vs		Vs		pch_lvt		l='Lmin'		w='3*Wp'
Mfap2		  Y			  B		  X		  X		  pch_lvt		l='Lmin'		w='3*Wp'
Mfap3		  CoutB		A		  Y		  Y		  pch_lvt		l='Lmin'		w='3*Wp'
Mfap4		  X			  B		  Vs		Vs		pch_lvt		l='Lmin'		w='2*Wp'
Mfap5		  CoutB		Cin		X		  X		  pch_lvt		l='Lmin'		w='2*Wp'
Mfan6		  Z			  A		  gnd		gnd		nch_lvt		l='Lmin'		w='2*Wn'
Mfan7		  Z			  B		  gnd		gnd		nch_lvt		l='Lmin'		w='2*Wn'
Mfan8		  CoutB		Cin		Z		  Z		  nch_lvt		l='Lmin'		w='2*Wn'
Mfan9		  W			  B		  gnd		gnd		nch_lvt		l='Lmin'		w='2*Wn'
Mfan10		CoutB		A		  W		  W		  nch_lvt		l='Lmin'		w='2*Wn'
Mfap11	  V		    A		  Vs		Vs		pch_lvt		l='Lmin'		w='4*Wp'
Mfap12		V		    B		  Vs		Vs		pch_lvt		l='Lmin'		w='4*Wp'
Mfap13		V		    Cin		Vs		Vs		pch_lvt		l='Lmin'		w='4*Wp'
Mfap14		U		    A		  V		  V		  pch_lvt		l='Lmin'		w='4*Wp'
Mfap15		T		    B		  U		  U		  pch_lvt		l='Lmin'		w='4*Wp'
Mfap16		SumB	  Cin		T		  T		  pch_lvt		l='Lmin'		w='4*Wp'
Mfap17		SumB	  CoutB	V		  V		  pch_lvt		l='Lmin'		w='0.66*Wp'
Mfan18		S		    A		  gnd		gnd		nch_lvt		l='Lmin'		w='2*Wn'
Mfan19		S		    B		  gnd		gnd		nch_lvt		l='Lmin'		w='2*Wn'
Mfan20		S		    Cin		gnd		gnd		nch_lvt		l='Lmin'		w='2*Wn'
Mfan21		R		    A		  gnd		gnd		nch_lvt		l='Lmin'		w='3*Wn'
Mfan22		Q		    B		  R		  R		  nch_lvt		l='Lmin'		w='3*Wn'
Mfan23		SumB	  Cin		Q		  Q		  nch_lvt		l='Lmin'		w='3*Wn'
Mfan24		SumB	  CoutB	S		  S		  nch_lvt		l='Lmin'		w='2*Wn'

XfaInv1	  CoutB		Cout	gnd		Vs		Inv
XfaInv2	  SumB		Sum		gnd		Vs		Inv
.ENDS	FA	

.SUBCKT   OneBitAdderHA   clk   X       Y         Sum         CO        gnd		Vs
XhaAddDff1	X		    XDff	clk		gnd		  Vs		    DFF
XhaAddDff2	Y		    YDff	clk		gnd		  Vs		    DFF
XaddHa		  XDff		YDff  CO		SumDff	gnd		    Vs		      HA
XhaAddDff3	SumDff	Sum		clk		gnd		  Vs		    DFF
.ENDS OneBitAdderHA

.SUBCKT   OneBitAdderFA   clk   X       Y         CIN     Sum     CO    gnd		Vs
XfaAddDff1	X		    XDff	clk		gnd		  Vs		    DFF
XfaAddDff2	Y		    YDff	clk		gnd		  Vs		    DFF
XaddFa	    XDff		YDff	CIN		CO		  SumDff	  gnd		  Vs		  FA
XfaAddDff3	SumDff	Sum		clk		gnd		  Vs		    DFF
.ENDS OneBitAdderFA

**** Circuit ****
Xa1 clk X0 Y0  Sum0 CO0       gnd		Vs OneBitAdderHA
Xa2 clk X1 Y1  CO0  Sum1 CO1  gnd		Vs OneBitAdderFA
Xa3 clk X2 Y2  CO1  Sum2 CO2  gnd		Vs OneBitAdderFA
Xa4 clk X3 Y3  CO2  Sum3 CO3  gnd		Vs OneBitAdderFA
Xa5 clk X4 Y4  CO3  Sum4 CO4  gnd		Vs OneBitAdderFA
Xa6 clk X5 Y5  CO4  Sum5 CO5  gnd		Vs OneBitAdderFA
Xa7 clk X6 Y6  CO5  Sum6 CO6  gnd		Vs OneBitAdderFA
Xa8 clk X7 Y7  CO6  Sum7 COO  gnd		Vs OneBitAdderFA

**** Analysis ****
.tran 	0.1ns	200n
.VEC 'CP.txt'

**** Measurements ****
.measure AVG_CURRENT
+avg i(VDD) from = 2n to 200n
.measure avg_power PARAM = ('AVG_CURRENT * VDD')
.measure tpdr
+ TRIG v(X0)    VAL=0.5   FALL=1
+ TARG v(Sum7)  VAL=0.5   RISE=1
.measure tpdf
+ TRIG v(X0)    VAL=0.5   RISE=1
+ TARG v(Sum7)  VAL=0.5   FALL=1
.measure tpd param='(tpdr+tpdf)/2'

.end